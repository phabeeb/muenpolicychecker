#include <math.h>
#include <inttypes.h> 
#include <string.h>
#include <stdlib.h>


#define SetBit(A,k)     ( A[(k/32)] |= (1 << (k%32)) )
#define ClearBit(A,k)   ( A[(k/32)] &= ~(1 << (k%32)) )            
#define TestBit(A,k)    ( A[(k/32)] & (1 << (k%32)) )

// Function to convert hexadecimal to decimal
uint64_t convtodecnum(char hex[]);
/*Format given Ada hex string to hex string*/
void formatHex(char *str);
/*Function to remove all occurrences of a character from the string.*/
void removeAll(char * str, const char toRemove);

void CheckStopOnError();
