/** 
 * purpose: Check correctness of toolchain generated muen image with individual elements.	
 * usage:	filename  <BPolicy-xmlfile> 
 * author : 
 */

#include <stdio.h>
#include <string.h>
#include <math.h>

#include <inttypes.h> 

#include <libxml/tree.h>
#include <libxml/parser.h>
#include <libxml/xpath.h>
#include <libxml/xpathInternals.h>

#include "commonFunctions.h"
#include "global.h"


/*Convert hexadecimal to decimal. */
uint64_t convtodecnum(char hex[]);
/* Removes all occurrence of toRemove from str*/
void removeAll(char * str, const char toRemove);
void formatHex(char *str);


int imageCheck(int argc, char *argv[]){

	xmlDocPtr Bpolicydoc;
	xmlXPathContextPtr BxpathCtx; 
	xmlXPathObjectPtr BxpathObjMemory,BxpathObjFile, BxpathObjFill, BxpathObjMemAll;
	//xmlNodePtr cur;
	xmlChar* BxpathExprMemory;
	xmlChar* BxpathExprFiles;
	xmlChar* BxpathExprFills;
	xmlChar* BxpathExprMemAll;
	xmlNodeSetPtr BmemoryElements, BfileElements, BfillElements, BmemAllElements;

	FILE* muenImage;

	int noOfMemElements, noOfFileElements, noOfFillElements, noOfMemAllElements;
	int64_t lowestPhyAddr, highestPhyAddr; /* Lowest and highest physical addresses.*/

    /* Init libxml */     
    xmlInitParser();
    LIBXML_TEST_VERSION

    /* Load XML document */
    Bpolicydoc = xmlParseFile(argv[2]);
    if (Bpolicydoc == NULL) {
    	fprintf(stderr, "Error: unable to parse file \"%s\"\n", argv[1]);
    	return -1;
    }

    /* Create xpath evaluation context */
    BxpathCtx = xmlXPathNewContext(Bpolicydoc);
    if(BxpathCtx == NULL) {
        fprintf(stderr,"Error: unable to create new XPath context\n");
        xmlFreeDoc(Bpolicydoc); 
        return -1;
    }

    /*Memory elements with file or fill */
    BxpathExprMemory="/system/memory/memory[file or fill]";

    /*Execute expression*/
   	BxpathObjMemory = xmlXPathEvalExpression(BxpathExprMemory, BxpathCtx);

    /*Validate  xpathobj*/
    if(BxpathObjMemory == NULL) {
        fprintf(stderr,"Error: unable to evaluate xpath expression \"%s\"\n", BxpathExprMemory);
        xmlXPathFreeContext(BxpathCtx); 
        xmlFreeDoc(Bpolicydoc); 
        return -1;
    }
    
    /*Retreive all elements */
    BmemoryElements = BxpathObjMemory->nodesetval;

    /* Number of memory elements with fill or file*/
    noOfMemElements = (BmemoryElements) ? BmemoryElements->nodeNr : 0;
    printf("\nNumber of Memory Elements (fill or file) = %d \n\n", noOfMemElements);

    lowestPhyAddr = 0;
    highestPhyAddr = 0;

    /* Assign first address as lowest address.*/
    char tempPhyAddr[100];
    strcpy(tempPhyAddr,  xmlGetProp(BmemoryElements->nodeTab[0], "physicalAddress"));
    formatHex(tempPhyAddr);     
    lowestPhyAddr = convtodecnum(tempPhyAddr);

    /* Findout lowest and highest physical addresses.*/
    char tempMemoryName[100],tempMemoryPAddr[100],tempMemorySize[100];
   	uint64_t phyAddrDecimal, sizeDecimal;

    for(int i = 0; i < noOfMemElements; i++) {    	
    	strcpy(tempMemoryName,  xmlGetProp(BmemoryElements->nodeTab[i], "name") );
    	strcpy(tempMemoryPAddr,  xmlGetProp(BmemoryElements->nodeTab[i], "physicalAddress") );
    	strcpy(tempMemorySize,  xmlGetProp(BmemoryElements->nodeTab[i], "size") );
    	//printf("\n %s\t", tempMemoryName);
    	//printf(" %s\t", tempMemoryPAddr);
    	//printf(" %s\t", tempMemorySize);
        
    	formatHex(tempMemoryPAddr);       
        phyAddrDecimal = convtodecnum(tempMemoryPAddr);

		formatHex(tempMemorySize);       
        sizeDecimal = convtodecnum(tempMemorySize);

        /* Find out lowest and highest physical addresses. */
        if(lowestPhyAddr > phyAddrDecimal)
        	lowestPhyAddr = phyAddrDecimal;
        if(highestPhyAddr < (phyAddrDecimal + sizeDecimal))
        	highestPhyAddr = phyAddrDecimal + sizeDecimal;
    }

    printf("Highest Physical Address: %" PRIu64 "\n", highestPhyAddr);
    printf("Lowest Physical Address : %" PRIu64 "\n", lowestPhyAddr);
	    
    if(system("xxd -p muenImage/muen.img > muenImage/muenImage_V1.txt") == -1){
       	printf("Muen file processing error V1.\n");
       	exit(0);                    
    }

    if(system("cat muenImage/muenImage_V1.txt | tr -d \" \\t\\n\\r\" > muenImage/muenImage_V2.txt ") == -1){
       	printf("Muen File processing V2 error.\n");
       	exit(0);                    
    }
    if(remove("muenImage/muenImage_V1.txt") == -1){
       	printf("Muen file remove error _V1.txt\n");
       	exit(0);
    }

    muenImage = fopen("muenImage/muenImage_V2.txt", "r");
	if (muenImage == NULL){
       	printf("Cannot open element file.  \n");
	 	exit(0);
    }


    printf("\nChecking File Elements\n");
   	printf("--------------------------\n");

    /* Validate file elements */

    /*Memory elements with file  */
    BxpathExprFiles = "/system/memory/memory[file]";

    /*Execute expression*/
   	BxpathObjFile = xmlXPathEvalExpression(BxpathExprFiles, BxpathCtx);

    /*Validate  xpathobj*/
    if(BxpathObjFile == NULL) {
        fprintf(stderr,"Error: unable to evaluate xpath expression \"%s\"\n", BxpathExprFiles);
        xmlXPathFreeContext(BxpathCtx); 
        xmlFreeDoc(Bpolicydoc); 
        return -1;
    }
    
    /*Retreive all file elements */
    BfileElements = BxpathObjFile->nodesetval;

    /* Number of file elements */
    noOfFileElements = (BfileElements) ? BfileElements->nodeNr : 0;
    printf("Number of File Elements = %d \n", noOfFileElements);


    char fileName[100], fileElemSize[100], fileOffset[100], filePhyAddr[100], fileElemName[100];
    int fileCheckFlag;
    char tempString3[100], tempString4[100], tempChar;
    xmlXPathObjectPtr tempBxpathObjFile;
   	xmlChar* tempBxpathExprFile;
   	xmlNodeSetPtr tempBFileElement;
   	uint64_t filePhyAddressDec, fileElemSizeDec, fileOffsetDec;
   	FILE* elementFile;

    for(int i = 0; i < noOfFileElements; i++) {
    	fileCheckFlag =0;

     	strcpy(fileElemName,  xmlGetProp(BfileElements->nodeTab[i], "name"));
    	strcpy(fileElemSize,  xmlGetProp(BfileElements->nodeTab[i], "size"));
    	strcpy(filePhyAddr,  xmlGetProp(BfileElements->nodeTab[i], "physicalAddress"));

    	formatHex(filePhyAddr);      
        filePhyAddressDec = convtodecnum(filePhyAddr);
		
		formatHex(fileElemSize);
        fileElemSizeDec = convtodecnum(fileElemSize);

        GtotalFileElementSize += fileElemSizeDec;

        sprintf(tempString3, "/system/memory/memory[@name='%s']/file", fileElemName);
        tempBxpathExprFile = tempString3;

        /*Execute expression*/
   		tempBxpathObjFile = xmlXPathEvalExpression(tempBxpathExprFile, BxpathCtx);

    	/*Validate  xpathobj*/
    	if(tempBxpathObjFile == NULL) {
        	fprintf(stderr,"Error: unable to evaluate xpath expression \"%s\"\n", tempBxpathExprFile);
        	xmlXPathFreeContext(BxpathCtx); 
        	xmlFreeDoc(Bpolicydoc); 
        	return -1;
    	}

     	/*Retreive all file elements */
    	tempBFileElement = tempBxpathObjFile->nodesetval;
    	strcpy(fileName,  xmlGetProp(tempBFileElement->nodeTab[0], "filename"));
    	strcpy(fileOffset,  xmlGetProp(tempBFileElement->nodeTab[0], "offset"));

    	printf("\n\n%d: ", i);
    	printf("ElementName : %s, ", fileElemName);
    	printf(" ElementPhyAddr : %s", filePhyAddr);
    	printf("(%" PRIu64 ")", filePhyAddressDec);
    	printf(", ElementSize : %s ", fileElemSize);
    	printf("(%" PRIu64 ") Bytes\n", fileElemSizeDec);
    	printf("FileName : %s, ", fileName);
    	printf("Offset : %s \n", fileOffset);

    	formatHex(fileOffset);       
        fileOffsetDec = convtodecnum(fileOffset);

        /* Translate files*/
        char cmd[100],cmd2[100],removeFileName[100], elementFileName[100];
        
        sprintf(cmd, "xxd -p files/%s > files/processedFiles/%s_V1.txt", fileName, fileName);             
       
     	if(system(cmd) == -1){
         	printf("File processing V1 error.\n");
         	exit(0);                    
         }

     	/* remove space and new line from processed file*/
        sprintf(cmd2, "cat files/processedFiles/%s_V1.txt | tr -d \" \\t\\n\\r\" > files/processedFiles/%s_V2.txt", fileName, fileName);             
        if(system(cmd2) == -1){
         	printf("File processing V2 error.\n");
         	exit(0);                    
        }

        
        /*Remove V1 of file.*/
        sprintf(removeFileName, "files/processedFiles/%s_V1.txt", fileName);             
        
        if(remove(removeFileName) == -1){
        	printf("File remove error _V1.txt\n");
        	exit(0);
        }  
     	
    	// printf("startin muen physicalAddress 2: %" PRIu64 "\n", ((filePhyAddressDec)-lowestPhyAddr)*2);
    	// printf("startin muen physicalAddress : %" PRIu64 "\n", ((filePhyAddressDec)-lowestPhyAddr));
    	// printf("offset*2 : %" PRIu64 "\n", fileOffsetDec*2);
    	// printf("offset : %" PRIu64 "\n", fileOffsetDec);
    	// printf("startin muen +offset 2 : %" PRIu64 "\n", ((filePhyAddressDec + fileOffsetDec)-lowestPhyAddr)*2);
    	// printf("startin muen +offset : %" PRIu64 "\n", ((filePhyAddressDec + fileOffsetDec)-lowestPhyAddr));
    	// //printf("fileStart2 : %" PRIu64 "\n", (fileElemSizeDec - fileOffsetDec)*2);
    	// //printf("fileStart : %" PRIu64 "\n", (fileElemSizeDec - fileOffsetDec));
    	// printf("fileElemSize*2 : %" PRIu64 "\n", (fileElemSizeDec*2));
    	// printf("fileElemSize : %" PRIu64 "\n", fileElemSizeDec);
    	// printf("startin muen +fileElemSize 2 : %" PRIu64 "\n", ((filePhyAddressDec + fileOffsetDec)-lowestPhyAddr + fileElemSizeDec)*2);
    	// printf("startin muen +fileElemSize  : %" PRIu64 "\n", ((filePhyAddressDec + fileOffsetDec)-lowestPhyAddr + fileElemSizeDec));
    	
    	
    	if(fseek(muenImage, ((filePhyAddressDec)-lowestPhyAddr)*2, SEEK_SET) != 0){
    		printf("File seek error !");
    		exit(0);
    	}

    	/* Open element file */
        sprintf(elementFileName, "files/processedFiles/%s_V2.txt", fileName);             
        
		elementFile = fopen(elementFileName, "r");
		if (elementFile == NULL){
         	printf("Cannot open element file.  \n");
		 	exit(0);
     	}


    	if(fileOffsetDec > 0){
    		if(fseek(elementFile, (fileOffsetDec)*2, SEEK_SET) != 0){
    			printf("File seek error !");
    			exit(0);
    		}
    	}

    	char c1,c2;
    	uint64_t j=0;
    	//printf("j : %" PRIu64 "\n", j);
    	
    	for (; j < (fileElemSizeDec )*2; ++j){	// = '0';

    		if((c2 = fgetc(elementFile))!= EOF){
    			c1 = fgetc(muenImage);
    		
    			if(c1 != c2){
    				fileCheckFlag ++;
    			}
    		}else{
    			//printf("breaking\n");
    			break;
    		}
    	}

    	while(j < (fileElemSizeDec)*2){

    		c1 = fgetc(muenImage);
    		
    		if(c1 != '0'){
    			fileCheckFlag ++;
    		}
    		j++;
    	}
    	//printf("j : %" PRIu64 "\n", j);
	
    	fclose(elementFile);
    	if(fileCheckFlag != 0){
    		printf("\n!!!!!File content mismatch found. No of missmatches %d\n", fileCheckFlag);
    		//printf("Do you whish to continue (y/n)");
		//	tempChar = getchar();
		//	if(tempChar != 'y' ){
		///	    return 0;
		//	}
		//	tempChar = getchar();
		CheckStopOnError();
    	}


    }

    printf("\nFile Check Finished.\n\n");
    printf("--------------------------\n");

    /* Validate fill elements */

    /*Memory elements with fill  */
    BxpathExprFills = "/system/memory/memory[fill]";

    /*Execute expression*/
   	BxpathObjFill = xmlXPathEvalExpression(BxpathExprFills, BxpathCtx);

    /*Validate  xpathobj*/
    if(BxpathObjFill == NULL) {
        fprintf(stderr,"Error: unable to evaluate xpath expression \"%s\"\n", BxpathExprFills);
        xmlXPathFreeContext(BxpathCtx); 
        xmlFreeDoc(Bpolicydoc); 
        return -1;
    }
    
    /*Retreive all fill elements */
    BfillElements = BxpathObjFill->nodesetval;

    /* Number of fill elements */
    noOfFillElements = (BfillElements) ? BfillElements->nodeNr : 0;
    printf("Number of Fill Elements = %d \n", noOfFillElements);

    
    char tempFillName[100], pattern[100], tempString1[100];
    char tempFillPhyAddr[100], tempFillSize[100];
   	xmlXPathObjectPtr tempBxpathObjFill;
   	xmlChar* tempBxpathExprFill;
   	xmlNodeSetPtr tempBFillElement;
   	uint64_t fillPhyAddressDec, fillSizeDec;
   	int fillCheckFlag = 0, len;

   	printf("\nChecking Fill Elements\n");
   	

    for(int i = 0; i < noOfFillElements; i++) {
    	strcpy(tempFillName,  xmlGetProp(BfillElements->nodeTab[i], "name"));
    	strcpy(tempFillPhyAddr,  xmlGetProp(BfillElements->nodeTab[i], "physicalAddress"));
    	strcpy(tempFillSize,  xmlGetProp(BfillElements->nodeTab[i], "size"));
  	
    	// printf("\n ElementName : %s\t", tempFillName);
    	// printf(" ElementPhyAddr : %s\t", tempFillPhyAddr);
    	// printf(" ElementSize : %s\t \n", tempFillSize);
    	
        sprintf(tempString1, "/system/memory/memory[@name='%s']/fill", tempFillName);
        tempBxpathExprFill = tempString1;

        /*Execute expression*/
   		tempBxpathObjFill = xmlXPathEvalExpression(tempBxpathExprFill, BxpathCtx);

    	/*Validate  xpathobj*/
    	if(tempBxpathObjFill == NULL) {
        	fprintf(stderr,"Error: unable to evaluate xpath expression \"%s\"\n", tempBxpathExprFill);
        	xmlXPathFreeContext(BxpathCtx); 
        	xmlFreeDoc(Bpolicydoc); 
        	return -1;
    	}
    
    	/*Retreive all fill elements */
    	tempBFillElement = tempBxpathObjFill->nodesetval;
    	strcpy(pattern,  xmlGetProp(tempBFillElement->nodeTab[0], "pattern"));

    	//printf(" %s\t\n", pattern);

    	formatHex(tempFillPhyAddr);       
        fillPhyAddressDec = convtodecnum(tempFillPhyAddr);

        formatHex(tempFillSize);
        fillSizeDec = convtodecnum(tempFillSize);

        GtotalFillElementSize += fillSizeDec;

        formatHex(pattern);
        
        printf("\nElementName : %s, ", tempFillName);
    	printf(" ElementPhyAddr : %s", tempFillPhyAddr);
    	printf("(%" PRIu64 ")", fillPhyAddressDec);
    	printf(", ElementSize : %s ", tempFillSize);
    	printf("(%" PRIu64 ") Bytes\n", fillSizeDec);
    	printf("Pattern %s \n", pattern);
    	
       // printf("%" PRIu64 "\n", fillPhyAddressDec);
       // printf("%" PRIu64 "\n", fillPhyAddressDec*2);

        if( fseek(muenImage, (fillPhyAddressDec-lowestPhyAddr)*2, SEEK_SET) != 0){
    		printf("File seek error !");
    		exit(0);
    	}
    
    	/* Check pattern in muen image. 
    		physicalAddress to size.*/
    	len = strlen(pattern);
    	int chunksize = len;
    	char *buffer = malloc(chunksize+1);
    	if (buffer == 0){
    			printf("Failed to allocate %d bytes memory\n", chunksize);
    			exit(0);
    	}
    	size_t nbytes;

    	for(int j = 0; j < fillSizeDec*2; j = j + len  ){
    	//for(int j = 0; j < 10*2; j = j + len  ){
    		nbytes = fread(buffer, 1, chunksize, muenImage);
			//fseek(src, 0, SEEK_SET);
			buffer[nbytes] = '\0';			
			if (nbytes != 0){
				if (strcmp(buffer, pattern) != 0){
					fillCheckFlag ++;
					printf("\t not match @");
					printf("offset %d \n", j );
					printf("\npattern : %s\t",pattern);
					printf("Read: %s", buffer);				
				}		
			}
    	}

    	free(buffer);
    }
    if(fillCheckFlag != 0){
    	printf("\nPattern mismatch found. Number of missmatches %d\n", fillCheckFlag);
    	//printf("Do you whish to continue (y/n)");
	//	tempChar = getchar();
	//	if(tempChar != 'y' ){
	//	    return 0;
	//	}
	//	tempChar = getchar();
	CheckStopOnError();
    }
    printf("\nFill Check Finished.\n");

    printf("-----------------------------------\n");
    /*Check memory elements without fill or file */
    printf("\nChecking memory elements without fillOrFile");

    /*Memory elements without file or fill */
    BxpathExprMemAll="/system/memory/memory[not (file or fill)]";

    /*Execute expression*/
   	BxpathObjMemAll = xmlXPathEvalExpression(BxpathExprMemAll, BxpathCtx);

    /*Validate  xpathobj*/
    if(BxpathObjMemAll == NULL) {
        fprintf(stderr,"Error: unable to evaluate xpath expression \"%s\"\n", BxpathExprMemAll);
        xmlXPathFreeContext(BxpathCtx); 
        xmlFreeDoc(Bpolicydoc); 
        return -1;
    }
    
    /*Retreive all elements */
    BmemAllElements = BxpathObjMemAll->nodesetval;

    /* Number of memory elements with fill or file*/
    noOfMemAllElements = (BmemAllElements) ? BmemAllElements->nodeNr : 0;
    printf("\nNumber of Memory Elements (without fill or file) = %d \n\n", noOfMemAllElements);


    char memElemName[100], memElemPhyAddr[100], memElemSize[100];
 	uint64_t memElemPhyAddrDec, memElemSizeDec;
   	int memElemAllCheckFlag;
    
    int num = 0;
    char c3;
    printf("Checking elements within range(>lowestPhyAddr && < highestPhyAddr)\n");

    for(int i = 0; i < noOfMemAllElements; i++) {
   	 	memElemAllCheckFlag = 0;

    	strcpy(memElemName,  xmlGetProp(BmemAllElements->nodeTab[i], "name"));
    	strcpy(memElemPhyAddr,  xmlGetProp(BmemAllElements->nodeTab[i], "physicalAddress"));
    	strcpy(memElemSize,  xmlGetProp(BmemAllElements->nodeTab[i], "size"));
		
		formatHex(memElemPhyAddr);      
        memElemPhyAddrDec = convtodecnum(memElemPhyAddr);

        formatHex(memElemSize);    
        memElemSizeDec = convtodecnum(memElemSize);


        if((memElemPhyAddrDec > lowestPhyAddr) && ((memElemPhyAddrDec + memElemSizeDec) < highestPhyAddr)){

        	printf("\n%d . ",num++ );
        	printf("ElementName : %s, ", memElemName);
    		printf(" ElementPhyAddr : %s", memElemPhyAddr);
    		printf("(%" PRIu64 ")", memElemPhyAddrDec);
    		printf(", ElementSize : %s ", memElemSize);
    		printf("(%" PRIu64 ") Bytes\n", memElemSizeDec);

    		if( fseek(muenImage, (memElemPhyAddrDec-lowestPhyAddr)*2, SEEK_SET) != 0){
    			printf("File seek error !");
    			exit(0);
    		}

    		for(int k = 0; k < (memElemSizeDec * 2); k = k + 1){
    			c3 = fgetc(muenImage);
    			if(c3 != '0'){
    				memElemAllCheckFlag++;
    			}
    		}
        }
        if(memElemAllCheckFlag != 0){
    		printf("\n!!!!!Non-zero file content found. Number of missmatches %d\n", memElemAllCheckFlag);
    		//printf("Do you whish to continue (y/n)");
		//	tempChar = getchar();
		//	if(tempChar != 'y' ){
		//	    return 0;
		//	}
		//	tempChar = getchar();
		CheckStopOnError();
    	}
  	}

  	printf("\nFinished \n");

    fclose(muenImage);
	return 0;
}


