#include <stdio.h>
#include <math.h>
#include "commonFunctions.h"
#include "global.h"
/*Format given Ada hex string to hex string*/
void formatHex(char *str){

	int len;
	len = strlen(str);
    memmove(str, str + 3, len - 3 + 1);
    //remove last character
    len = strlen(str);
    str[len-1] = '\0';    
    //remove all occurrence of _
    removeAll(str, '_');   
}

/**
 * Function to remove all occurrences of a character from the string.
 */
void removeAll(char * str, const char toRemove)
{
    int i, j;
    int len = strlen(str);

    for(i=0; i<len; i++)
    {
        /*
         * If the character to remove is found then shift all characters to one
         * place left and decrement the length of string by 1.
         */
        if(str[i] == toRemove)
        {
            for(j=i; j<len; j++)
            {
                str[j] = str[j+1];
            }

            len--;

            // If a character is removed then make sure i doesn't increments
            i--;
        }
    }
}

// Function to convert hexadecimal to decimal
uint64_t convtodecnum(char hex[])
{
    char *hexstr;
    int64_t length = 0;
    const int base = 16; // Base of Hexadecimal Number
    uint64_t decnum = 0;
    int64_t i;
    uint64_t val, len, temp;

    len = strlen(hex);
    len--;

    /*
     * Iterate over each hex digit
     */
    for(i=0; hex[i]!='\0'; i++)
    {
 
       /* Find the decimal representation of hex[i] */
        if(hex[i]>='0' && hex[i]<='9')
        {
            val = hex[i] - 48;
        }
        else if(hex[i]>='a' && hex[i]<='f')
        {
            val = hex[i] - 97 + 10;
        }
        else if(hex[i]>='A' && hex[i]<='F')
        {
            val = hex[i] - 65 + 10;
        }
	temp = val * pow(16, len);
        decnum += temp;
        len--;
    }
    return decnum;
}

void CheckStopOnError(){
    char tempChar;
    //printf("%s\n", message);
    if(stopOnError == 1){
        printf("Do you wish to continue (y/n)");
            tempChar = getchar();
            if(tempChar == 'n' ){
                 //return 0;
                exit(0);
            }
            tempChar = getchar();
    }
}
