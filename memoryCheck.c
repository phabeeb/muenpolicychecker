
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <inttypes.h> 

#include <libxml/tree.h>
#include <libxml/parser.h>
#include <libxml/xpath.h>
#include <libxml/xpathInternals.h>

#include "commonFunctions.h"
#include "memoryCheck.h"
#include "global.h"

// #define SetBit(A,k)     ( A[(k/32)] |= (1 << (k%32)) )
// #define ClearBit(A,k)   ( A[(k/32)] &= ~(1 << (k%32)) )            
// #define TestBit(A,k)    ( A[(k/32)] & (1 << (k%32)) )

long long int maxSizeOfPT;/*maximum size of page table*/
int *PTbitarray;


int memoryCheck(int argc, char *argv[])
{
	/* code */

	xmlDocPtr Upolicydoc, Bpolicydoc;
    xmlXPathContextPtr UxpathCtx,BxpathCtx; 

    struct MemNode *start = NULL;
    struct SubjectNode *SubjectStart = NULL;
    struct SubjectNode *KernelStart = NULL;

	int numberOfMemoryElements, numberOfSubjects, numberOfCpu;

    long long int tempLLD;
    const uint64_t Bit_0 = 0x0000000000000001;

    /* Init libxml */     
    xmlInitParser();
    LIBXML_TEST_VERSION

    /* Load XML document */
    Upolicydoc = xmlParseFile(argv[1]);
    if (Upolicydoc == NULL) {
    	fprintf(stderr, "Error: unable to parse file \"%s\"\n", argv[1]);
    	return -1;
    }

   /* Load XML document */
    Bpolicydoc = xmlParseFile(argv[2]);
    if (Bpolicydoc == NULL) {
    	fprintf(stderr, "Error: unable to parse file \"%s\"\n", argv[2]);
    	return -1;
    }

    /* Create xpath evaluation context */
    UxpathCtx = xmlXPathNewContext(Upolicydoc);
    if(UxpathCtx == NULL) {
        fprintf(stderr,"Error: unable to create new XPath context\n");
        xmlFreeDoc(Upolicydoc); 
        return -1;
    }

    /* Create xpath evaluation context */
    BxpathCtx = xmlXPathNewContext(Bpolicydoc);
    if(BxpathCtx == NULL) {
        fprintf(stderr,"Error: unable to create new XPath context\n");
        xmlFreeDoc(Bpolicydoc); 
        return -1;
    }

    /* started: Checking memory overlap for all system/memory elements */
    ////////////////////////////////////////////////////////////////////////////////////////////////////
  
  	xmlChar* BxpathExprWholeMem;
  	xmlXPathObjectPtr BxpathObjWholeMem;
  	xmlNodeSetPtr BWholeMemory;
  	char tName[100],tPhyAddr[100],tSize[100];
  	int64_t phyAddrDec,sizeDec;
  	struct MemNode* current;
  	char tempChar;

    BxpathExprWholeMem="/system/memory/memory[@name]";
    BxpathObjWholeMem = xmlXPathEvalExpression(BxpathExprWholeMem, BxpathCtx);

    if(BxpathObjWholeMem == NULL) {
        fprintf(stderr,"Error: unable to evaluate xpath expression \"%s\"\n", BxpathExprWholeMem);
        xmlXPathFreeContext(BxpathCtx); 
        xmlFreeDoc(Bpolicydoc); 
        return -1;
    }

    BWholeMemory= BxpathObjWholeMem->nodesetval;

    numberOfMemoryElements =(BWholeMemory) ? BWholeMemory->nodeNr : 0;
    printf("Number of Physical Memory Elements = %d \n\n", numberOfMemoryElements);
    printf(">>>Creating Physical Memory Element Linked List\n");

    //creating linked list of memory elements
    for(int i=0; i<numberOfMemoryElements; i++){
        strcpy(tName,  xmlGetProp(BWholeMemory->nodeTab[i], "name") );
        strcpy(tPhyAddr,  xmlGetProp(BWholeMemory->nodeTab[i], "physicalAddress") ) ;
        strcpy(tSize,  xmlGetProp(BWholeMemory->nodeTab[i], "size") ) ;
        
        formatHex(tPhyAddr);        
        phyAddrDec = convtodecnum(tPhyAddr);
        
        formatHex(tSize);
        sizeDec = convtodecnum(tSize);
        
        GtotalMemorySize += sizeDec;

        insertAtTheBegin(&start,tName,phyAddrDec,sizeDec);

    }
   
    printf(">>>Sorting memory elements based on physical address\n" );
    bubbleSort(start);
    printf(">>>Sorting Completed\n");
    printf(">>>Checking memory overlap\n");

    current = start; 
    while ((current != NULL ) && (current->next != NULL))
    {
         if((current->physical_address + current->size-1) >= (current->next->physical_address)){
            printf("Memory segments overlapped : %s , %s\n", current->physical_name,current->next->physical_name);
            // printf("Do you wish to continue (y/n)");
            // tempChar = getchar();
            // if(tempChar != 'y' ){
            //      return 0;
            // }
            // tempChar = getchar();
            CheckStopOnError();
         }
        current = current->next;
    }
  
    printf("Overlap Check Finished\n");

    /* Finished: checking memory overlap for all system/memory elements */
    ///////////////////////////////////////////////////////////////////////////////////////////////////

    /* for all valid subject address 'a', Checking tr(s,a) = pt(s,a), where s = subject number.  */
    //////////////////////////////////////////////////////////////////////////////////////////////////

    printf("\n>>>For all valid address 'a', Checking tr(s,a) = pt(s,a), where s = subject number. \n");
    
    xmlChar* subjectsExpr, *cr3Expr;
    xmlChar *subjectExpr, *subjPhyAddrExpr, *subjectTypeExpr;
    xmlXPathObjectPtr subjectsObj, subjectObj, subjMemObj, subjPhyAddrObj, cr3Obj, subjTypeObj, singleObj;
    xmlNodeSetPtr BsubjectsElement, subjectMemElement, singleMemory, EPTElement;
    FILE *subjectPageTable;
    struct SubjectNode* CurrentSubjectNode;
    struct SubjectNode* tempSubjectNode;

    int number_of_elements;
    char TLogicalName[100], TPhysicalName[100], TWritable[10], TExecutable[10];
    uint64_t TLogicalAddress, TPhysicalAddress, TSize, subjectCr3;
    int writable, executable;
    
    char tempString1[100],tempString2[100];

    subjectsExpr="/system/subjects/subject[@id]";
    subjectsObj = xmlXPathEvalExpression(subjectsExpr, BxpathCtx);

    if(subjectsObj == NULL) {
        fprintf(stderr,"Error: unable to evaluate xpath expression \"%s\"\n", subjectsExpr);
        xmlXPathFreeContext(BxpathCtx); 
        xmlFreeDoc(Bpolicydoc); 
        return -1;
    }

    BsubjectsElement= subjectsObj->nodesetval;    
    
    numberOfSubjects =(BsubjectsElement) ? BsubjectsElement->nodeNr : 0;
    printf("Number of Subjects = %d \n", numberOfSubjects);

    GnoOfSubjects = numberOfSubjects;

    subjectIdName =(struct subject_id_name *) malloc(numberOfSubjects * sizeof(struct subject_id_name));

    maxSizeOfPT = 0;
    for(int i = 0; i < numberOfSubjects; ++i) {    
	    subjectIdName[i].id = atoi(xmlGetProp(BsubjectsElement->nodeTab[i], "id"));
        strcpy(subjectIdName[i].name,  xmlGetProp(BsubjectsElement->nodeTab[i], "name") );  
        subjectIdName[i].cpu =  atoi(xmlGetProp(BsubjectsElement->nodeTab[i], "cpu")) ; 

        sprintf(tempString1, "/system/subjects/subject[@id='%d']/vcpu/vmx/controls/proc2/EnableEPT", i);

        subjectTypeExpr=tempString1;
        subjTypeObj = xmlXPathEvalExpression(subjectTypeExpr, BxpathCtx);

        if(subjTypeObj == NULL) {
            fprintf(stderr,"Error: unable to evaluate xpath expression \"%s\"\n", subjectTypeExpr);
            xmlXPathFreeContext(BxpathCtx); 
             xmlFreeDoc(Bpolicydoc); 
            return -1;
        }

        EPTElement= subjTypeObj->nodesetval;
        subjectIdName[i].type =  atoi(xmlNodeListGetString(Bpolicydoc, EPTElement->nodeTab[0]->xmlChildrenNode,1));
	    //  printf("%d  ", subjectIdName[i].id );
	    //  printf("%s ", subjectIdName[i].name );
	    //  printf("%d\n", subjectIdName[i].cpu); 
	  	// printf("%d\n", subjectIdName[i].type); 

        /*Raw binary to text format.*/
    	sprintf(tempString1, "xxd -p files/%s_pt  > files/processedFiles/%s_pt_V1.txt ", 
    		subjectIdName[i].name,subjectIdName[i].name); 
    	if(system(tempString1) == -1){
         	printf("File processing V1 error.\n");
         	exit(0);                    
        }     	

     	/* remove space and new line from processed file*/
     	char tempString10[200];
     	sprintf(tempString10, "cat files/processedFiles/%s_pt_V1.txt |  tr -d \" \\t\\n\\r\" > files/processedFiles/%s_pt_V2.txt ", 
         	subjectIdName[i].name, subjectIdName[i].name);
         	//printf("%s\n", tempString10 );             
        if(system(tempString10) == -1){
         	printf("File processing V2 error.\n");
         	exit(0);                    
        }
             
        /*Remove V1 of file.*/
        sprintf(tempString1, "files/processedFiles/%s_pt_V1.txt", subjectIdName[i].name);                     
        if(remove(tempString1) == -1){
        	printf("File remove error _V1.txt\n");
        	exit(0);
        } 

        /*maximum size of pt*/
        sprintf(tempString1, "system/memory/memory[@name='%s|pt']", subjectIdName[i].name);
        singleObj= xmlXPathEvalExpression(tempString1, BxpathCtx);
        if(singleObj == NULL) {
                fprintf(stderr,"Error: unable to evaluate xpath expression \"%s\"\n", tempString1);
                xmlXPathFreeContext(BxpathCtx); 
                xmlFreeDoc(Bpolicydoc); 
                return -1;
        }
        singleMemory= singleObj->nodesetval;
               
        strcpy(tempString1,xmlGetProp(singleMemory->nodeTab[0], "size"));
        formatHex(tempString1);
        tempLLD = convtodecnum(tempString1);

        subjectIdName[i].ptSize = tempLLD;

        if(maxSizeOfPT < tempLLD)
            maxSizeOfPT = tempLLD;    


    }
   

    /*create bit array of size maxSizeOfPT*/
    PTbitarray = (int *)malloc(sizeof(int)*(maxSizeOfPT/8));
    memset(PTbitarray,0,sizeof(int)*(maxSizeOfPT/8)); 








   	for(int k = 0; k < numberOfSubjects ; ++k) {
    //for(int k = 1; k < numberOfSubjects ; ++k) {
    
    	sprintf(tempString1, "system/subjects/subject[@id='%d']/memory/memory", k);
   	    subjectExpr=tempString1;
        subjMemObj = xmlXPathEvalExpression(subjectExpr, BxpathCtx);

        if(subjMemObj == NULL) {
            fprintf(stderr,"Error: unable to evaluate xpath expression \"%s\"\n", subjectExpr);
            xmlXPathFreeContext(BxpathCtx); 
            xmlFreeDoc(Bpolicydoc); 
            return -1;
        }
        subjectMemElement= subjMemObj->nodesetval;   
        number_of_elements =(subjectMemElement) ? subjectMemElement->nodeNr : 0;

		
		/*Get cr3 */
        sprintf(tempString1, "system/memory/memory[@name='%s|pt']",subjectIdName[k].name);
        cr3Expr=tempString1;
        cr3Obj= xmlXPathEvalExpression(cr3Expr, BxpathCtx);
        if(cr3Obj == NULL) {
            fprintf(stderr,"Error: unable to evaluate xpath expression \"%s\"\n", cr3Expr);
            xmlXPathFreeContext(BxpathCtx); 
            xmlFreeDoc(Bpolicydoc); 
            return -1;
        }
        singleMemory= cr3Obj->nodesetval;
      	strcpy(tempString1,xmlGetProp(singleMemory->nodeTab[0], "physicalAddress")); 
     	formatHex(tempString1);
//printf("cr3  %s\n",tempString1 );
     	subjectCr3 = convtodecnum(tempString1);

        /*Open pagetable file*/
		sprintf(tempString1, "files/processedFiles/%s_pt_V2.txt", subjectIdName[k].name);                     
        subjectPageTable = fopen(tempString1, "r");
		if (subjectPageTable == NULL){
       		printf("Cannot open file.  \n");
	 		exit(0);
    	}

//printf("%s\n", tempString1 ); 
//printf("LogAddr: %" PRIu64 "", getPageTableEntry(672*8, subjectPageTable));

//printf("@0: %" PRIu64 "", getPageTableEntry(0*8, subjectPageTable));
//printf("@28672: %" PRIu64 "", getPageTableEntry(28672, subjectPageTable));
//exit(0);


    	printf("\nChecking subject : %s\n",  subjectIdName[k].name );

         memset(PTbitarray,0,sizeof(int)*(maxSizeOfPT/256)); 
        for(int l=0; l<number_of_elements;l++){
    	
            strcpy(TLogicalName,xmlGetProp(subjectMemElement->nodeTab[l], "logical"));
	        strcpy(TPhysicalName, xmlGetProp(subjectMemElement->nodeTab[l], "physical"));
            
    	    strcpy(tempString1,xmlGetProp(subjectMemElement->nodeTab[l], "virtualAddress"));
    	    formatHex(tempString1);
            TLogicalAddress = convtodecnum(tempString1); 

            //Physical_Address
            sprintf(tempString1, "system/memory/memory[@name='%s']", xmlGetProp(subjectMemElement->nodeTab[l], "physical"));
            subjPhyAddrExpr=tempString1;
            subjPhyAddrObj= xmlXPathEvalExpression(subjPhyAddrExpr, BxpathCtx);
            if(subjPhyAddrObj == NULL) {
                fprintf(stderr,"Error: unable to evaluate xpath expression \"%s\"\n", subjPhyAddrExpr);
                xmlXPathFreeContext(BxpathCtx); 
                xmlFreeDoc(Bpolicydoc); 
                return -1;
            }
            singleMemory= subjPhyAddrObj->nodesetval;
               
	        strcpy(tempString1,xmlGetProp(singleMemory->nodeTab[0], "physicalAddress"));
   			formatHex(tempString1);
   			TPhysicalAddress = convtodecnum(tempString1);
  
            strcpy(tempString1,xmlGetProp(singleMemory->nodeTab[0], "size"));
   			formatHex(tempString1);
            TSize = convtodecnum(tempString1);

            strcpy(TExecutable, xmlGetProp(subjectMemElement->nodeTab[l], "executable"));
            strcpy(TWritable,xmlGetProp(subjectMemElement->nodeTab[l], "writable"));
            executable = 0;
            writable = 0;
            if(!(strcmp(xmlGetProp(subjectMemElement->nodeTab[l], "executable"), "true"))){
            	executable = 1;
            }

            if(!(strcmp(xmlGetProp(subjectMemElement->nodeTab[l], "writable"), "true"))){
            	writable = 1;
            }


           uint64_t currPhyAddress = TPhysicalAddress;
           uint64_t currLogAddress = TLogicalAddress;

           printf("\n%d. LogicalName: %s, PhysicalName: %s,", l,TLogicalName, TPhysicalName );

           printf("LogAddr: %" PRIu64 "", TLogicalAddress);
           printf("PhyAddr: %" PRIu64 "", TPhysicalAddress);
           printf("Size: %" PRIu64 "\n\n", TSize);

           while(currLogAddress < TLogicalAddress + TSize){

            	if( (translateAddress(currLogAddress, subjectPageTable, subjectCr3, writable,executable, 
            		subjectIdName[k].type )) != (currPhyAddress)){
            			
            			printf("!!!!!Address translation error. currLogAddress%" PRIu64 ", ", currLogAddress);
            			printf("currentPhysicalAddr: %" PRIu64 "", translateAddress(currLogAddress, subjectPageTable, subjectCr3,  
            				 writable, executable, subjectIdName[k].type));
            			printf(", Expected physicalAddress%" PRIu64 "\n\n ", currPhyAddress);
            // 			printf("Do you wish to continue (y/n)");
        				// tempChar = getchar();
           	// 			if(tempChar != 'y' ){
            //      				return 0;
            // 			}
            // 			tempChar = getchar();
                        CheckStopOnError();
            	}
            	currLogAddress = currLogAddress + 4096;
            	currPhyAddress = currPhyAddress + 4096;
           }

           insertSubjectNode(&SubjectStart, TPhysicalName,TPhysicalAddress,
           				TSize,TLogicalName,TLogicalAddress,TWritable,TExecutable,k);      
        }

       // printf("TEST BIT %d",TestBit(PTbitarray,0));
       // 

        //printf("Content: %" PRIu64 "", getPageTableEntry(0, subjectPageTable));
       // printf("Answer : %d\n",(getPageTableEntry(0, subjectPageTable) != 0 ) && (TestBit(PTbitarray,0) ==0) );
        

        for(long long int x=0; x < subjectIdName[k].ptSize; x=x+8 ){
            if( (getPageTableEntry(x, subjectPageTable) != 0 ) && (TestBit(PTbitarray,(x/8)) ==0)){
                
               // printf(" Present bit %lud \n",); 
                if(getPageTableEntry(x, subjectPageTable)  & Bit_0){
                   // printf("LogAddr: %" PRIu64 "", getPageTableEntry(x, subjectPageTable));
                    printf("\n!!!!Invalid Page Table Entry @offset %lld\n", x/8);

                    CheckStopOnError();
                }

            }
        }
        /// exit(0);

       fclose(subjectPageTable);
    }
    
    printf("\n>>>Checking Subject Memory segments overlap\n>>>Sorting Subjects memory elements based on physical address\n" );
    bubbleSortSubjectNode(SubjectStart);
	printf("\n\n>>>Checking Subject memory Elements overlap\n");

    CurrentSubjectNode = SubjectStart; 
    while ((CurrentSubjectNode != NULL ) && (CurrentSubjectNode->Next != NULL))
    {
         if((CurrentSubjectNode->PhysicalAddress + CurrentSubjectNode->Size-1) >= (CurrentSubjectNode->Next->PhysicalAddress)){

         	if((isChannelSegment(CurrentSubjectNode->PhysicalName, UxpathCtx ) != 1) || 
         		(isChannelSegment(CurrentSubjectNode->Next->PhysicalName, UxpathCtx) != 1)
                ||(((strcmp(CurrentSubjectNode->PhysicalName ,CurrentSubjectNode->Next->PhysicalName))) )
                ){

                if(//(!(strcmp(CurrentSubjectNode->PhysicalName ,CurrentSubjectNode->Next->PhysicalName)))||
                    (strcmp(CurrentSubjectNode->Writable, "false")  ) ||( strcmp(CurrentSubjectNode->Next->Writable,"false") )
                    ){
                    printf("Memory segments overlapped : %s , %s\n", 
                    CurrentSubjectNode->PhysicalName,CurrentSubjectNode->Next->PhysicalName);
                    printf("subject1 %d, subject2 %d \n", CurrentSubjectNode->SubjectID,CurrentSubjectNode->Next->SubjectID );
                // printf("Do you whish to continue (y/n)");
                // tempChar = getchar();
                // if(tempChar != 'y' ){
                //      return 0;
                // }
                // tempChar = getchar();
                   
                CheckStopOnError();
                }
			    
        	}

         }
        CurrentSubjectNode = CurrentSubjectNode->Next;
    }
  
     printf("\nOverlap Check Finished\n");

    /*Finished: Checking memory overlap between subjects. (Except channels.)*/
    //////////////////////////////////////////////////////////////////////////////////////////////////

    /* for all valid kernel address 'a', Checking tr(k,a) = pt(k,a), where k = kernel number.  */

    printf("\n>>>for all valid kernel address 'a', Checking tr(k,a) = pt(k,a), where k = kernel number.\n" );

    xmlChar* BxpathExpr0;
    xmlXPathObjectPtr BxpathObj0;
    xmlNodeSetPtr BKernelElements;

    xmlXPathObjectPtr kernelMemObj, kerenelPhyAddrObj;
    xmlNodeSetPtr kernelMemElement;

    int noOfKernelMemElements;
    uint64_t kernelCr3;
    FILE *kernelPageTable;

    //Number of CPUs
    BxpathExpr0="/system/kernel/memory/cpu[@id]";
    BxpathObj0 = xmlXPathEvalExpression(BxpathExpr0, BxpathCtx);

    if(BxpathObj0 == NULL) {
        fprintf(stderr,"Error: unable to evaluate xpath expression \"%s\"\n", BxpathExpr0);
        xmlXPathFreeContext(BxpathCtx); 
        xmlFreeDoc(Bpolicydoc); 
        return -1;
    }
    BKernelElements= BxpathObj0->nodesetval;   
    numberOfCpu=(BKernelElements) ? BKernelElements->nodeNr : 0;

    GnoOfCPUs = numberOfCpu;

    for(int i=0; i < numberOfCpu; i++ ){
    	/*Raw binary to text format.*/
		sprintf(tempString1, "xxd -p files/kernel_pt_%d  > files/processedFiles/kernel_pt_%d_V1.txt ", i, i); 
		if(system(tempString1) == -1){
	     	printf("File processing V1 error.\n");
	     	exit(0);                    
	    }     	

	 	// /* remove space and new line from processed file*/
	 	char tempString10[200];
	 	sprintf(tempString10, "cat files/processedFiles/kernel_pt_%d_V1.txt |  tr -d \" \\t\\n\\r\" \
	 		> files/processedFiles/kernel_pt_%d_V2.txt ", i, i);
	     	//printf("%s\n", tempString10 );             
	    if(system(tempString10) == -1){
	     	printf("File processing V2 error.\n");
	     	exit(0);                    
	    }
	         
	    /*Remove V1 of file.*/
	    sprintf(tempString1, "files/processedFiles/kernel_pt_%d_V1.txt", i);                     
	    if(remove(tempString1) == -1){
	    	printf("File remove error _V1.txt\n");
	    	exit(0);
	    } 

	    sprintf(tempString1, "system/kernel/memory/cpu[@id='%d']/memory", i);   		
        kernelMemObj = xmlXPathEvalExpression(tempString1, BxpathCtx);

        if(kernelMemObj == NULL) {
            fprintf(stderr,"Error: unable to evaluate xpath expression \"%s\"\n", tempString1);
            xmlXPathFreeContext(BxpathCtx); 
            xmlFreeDoc(Bpolicydoc); 
            return -1;
        }
        kernelMemElement= kernelMemObj->nodesetval;   
        noOfKernelMemElements =(kernelMemElement) ? kernelMemElement->nodeNr : 0;
		
		/*Get cr3 */        
        sprintf(tempString1, "system/memory/memory[@name='kernel_%d|pt']", i);
        cr3Expr=tempString1;
        cr3Obj= xmlXPathEvalExpression(cr3Expr, BxpathCtx);
        if(cr3Obj == NULL) {
            fprintf(stderr,"Error: unable to evaluate xpath expression \"%s\"\n", cr3Expr);
            xmlXPathFreeContext(BxpathCtx); 
            xmlFreeDoc(Bpolicydoc); 
            return -1;
        }
        singleMemory= cr3Obj->nodesetval;
      	strcpy(tempString1,xmlGetProp(singleMemory->nodeTab[0], "physicalAddress")); 
        formatHex(tempString1);
     	kernelCr3 = convtodecnum(tempString1);

        /*Open pagetable file*/
		sprintf(tempString1, "files/processedFiles/kernel_pt_%d_V2.txt", i);                     
        kernelPageTable = fopen(tempString1, "r");
		if (kernelPageTable == NULL){
       		printf("Cannot open file.  \n");
	 		exit(0);
    	}
 
    	printf("\nChecking Kernel : %d\n", i );

        for(int l=0; l<noOfKernelMemElements;l++){
    	
            strcpy(TLogicalName,xmlGetProp(kernelMemElement->nodeTab[l], "logical"));
	        strcpy(TPhysicalName, xmlGetProp(kernelMemElement->nodeTab[l], "physical"));            
    	    strcpy(tempString1,xmlGetProp(kernelMemElement->nodeTab[l], "virtualAddress"));
    	    formatHex(tempString1);
            TLogicalAddress = convtodecnum(tempString1); 

            //Physical_Address 
            sprintf(tempString1, "system/memory/memory[@name='%s']", xmlGetProp(kernelMemElement->nodeTab[l], "physical"));
            kerenelPhyAddrObj= xmlXPathEvalExpression(tempString1, BxpathCtx);
            if(kerenelPhyAddrObj == NULL) {
                fprintf(stderr,"Error: unable to evaluate xpath expression \"%s\"\n", tempString1);
                xmlXPathFreeContext(BxpathCtx); 
                xmlFreeDoc(Bpolicydoc); 
                return -1;
            }
            singleMemory= kerenelPhyAddrObj->nodesetval;
               
	        strcpy(tempString1,xmlGetProp(singleMemory->nodeTab[0], "physicalAddress"));
   			formatHex(tempString1);
   			TPhysicalAddress = convtodecnum(tempString1);
  
            strcpy(tempString1,xmlGetProp(singleMemory->nodeTab[0], "size"));
   			formatHex(tempString1);
            TSize = convtodecnum(tempString1);

            GtotalKernelMemorySize += TSize;

            strcpy(TExecutable, xmlGetProp(kernelMemElement->nodeTab[l], "executable"));
            strcpy(TWritable,xmlGetProp(kernelMemElement->nodeTab[l], "writable"));
            executable = 0;
            writable = 0;
            if(!(strcmp(xmlGetProp(kernelMemElement->nodeTab[l], "executable"), "true"))){
            	executable = 1;
            }

            if(!(strcmp(xmlGetProp(kernelMemElement->nodeTab[l], "writable"), "true"))){
            	writable = 1;
            }

           uint64_t currPhyAddress = TPhysicalAddress;
           uint64_t currLogAddress = TLogicalAddress;

           printf("\n%d. LogicalName: %s, PhysicalName: %s,", l,TLogicalName, TPhysicalName );

           printf("LogAddr: %" PRIu64 "", TLogicalAddress);
           printf("PhyAddr: %" PRIu64 "", TPhysicalAddress);
           printf("Size: %" PRIu64 "\n\n", TSize);
           while(currLogAddress < TLogicalAddress + TSize){
            	if( (translateAddress(currLogAddress, kernelPageTable, kernelCr3, writable,executable, 
            		0 )) != (currPhyAddress)){
            			
            			printf("!!!!!Address translation error. currLogAddress%" PRIu64 ", ", currLogAddress);
            			printf("currentPhysicalAddr: %" PRIu64 "", translateAddress(currLogAddress, kernelPageTable, kernelCr3,  
            				 writable, executable, 0));
            			printf(", Expected physicalAddress%" PRIu64 "\n\n ", currPhyAddress);
            // 			printf("Do you wish to continue (y/n)");
        				// tempChar = getchar();
           	// 			if(tempChar != 'y' ){
            //      				return 0;
            // 			}
            // 			tempChar = getchar();
                        CheckStopOnError();
            	}
            	currLogAddress = currLogAddress + 4096;
            	currPhyAddress = currPhyAddress + 4096;

            }


            insertSubjectNode(&KernelStart, TPhysicalName,TPhysicalAddress,
                        TSize,TLogicalName,TLogicalAddress,TWritable,TExecutable,i);  


        }
       fclose(kernelPageTable);	}
    printf("\n kernel address Check Finished\n");
    /*finished: for all valid kernel address 'a', Checking tr(k,a) = pt(k,a), where k = kernel number.  */

    printf("\n>>>Checking Kernel Memory segments overlap\n>>>Sorting kernel memory elements based on physical address\n" );
    bubbleSortSubjectNode(KernelStart);
    printf("\n\n>>>Checking Kernel memory Elements overlap\n");

    CurrentSubjectNode = KernelStart; 
    while ((CurrentSubjectNode != NULL ) && (CurrentSubjectNode->Next != NULL))
    {
         if((CurrentSubjectNode->PhysicalAddress + CurrentSubjectNode->Size-1) >= (CurrentSubjectNode->Next->PhysicalAddress)){

            // if((isChannelSegment(CurrentSubjectNode->PhysicalName, UxpathCtx ) != 1) || 
            //     (isChannelSegment(CurrentSubjectNode->Next->PhysicalName, UxpathCtx) != 1)
            //     ||(((strcmp(CurrentSubjectNode->PhysicalName ,CurrentSubjectNode->Next->PhysicalName))) )
            //     ){

                if(//(!(strcmp(CurrentSubjectNode->PhysicalName ,CurrentSubjectNode->Next->PhysicalName)))||
                    (strcmp(CurrentSubjectNode->Writable, "false")  ) ||( strcmp(CurrentSubjectNode->Next->Writable,"false") )
                    ){
                    printf("Memory segments overlapped : %s , %s\n", 
                    CurrentSubjectNode->PhysicalName,CurrentSubjectNode->Next->PhysicalName);
                    printf("Kernel1 %d, Kernel2 %d \n", CurrentSubjectNode->SubjectID,CurrentSubjectNode->Next->SubjectID );
                // printf("Do you whish to continue (y/n)");
                // tempChar = getchar();
                // if(tempChar != 'y' ){
                //      return 0;
                // }
                // tempChar = getchar();
                   
                CheckStopOnError();
                }
                
          //  }

         }
        CurrentSubjectNode = CurrentSubjectNode->Next;
    }
  
     printf("\nOverlap Check Finished\n");

	return 0;
}

/* Function to insert a node at the begining of a linked lsit */
void insertAtTheBegin(struct MemNode **start_ref, char physical_name[100],int64_t physical_address, int64_t size)
{
    struct MemNode *ptr1 = (struct MemNode*)malloc(sizeof(struct MemNode));
    strcpy(ptr1->physical_name ,physical_name);
    ptr1->physical_address = physical_address;
    ptr1->size = size;
    ptr1->next = *start_ref;
    *start_ref = ptr1;
}


/* Function to print nodes in a given linked list */
void printList(struct MemNode *start)
{
    struct MemNode *temp = start;
    //printf("\n");
    while (temp!=NULL)
    {
        printf("%s\n", temp->physical_name);
        temp = temp->next;
    }
}

/* Bubble sort the given linked lsit */
void bubbleSort(struct MemNode *start)
{
    int swapped, i;
    struct MemNode *ptr1;
    struct MemNode *lptr = NULL;
 
    /* Checking for empty list */
    if (start == NULL)
        return;
 
    do
    {
        swapped = 0;
        ptr1 = start;
 
        while (ptr1->next != lptr)
        {
            if (ptr1->physical_address >= ptr1->next->physical_address)
            { 
                swap(ptr1, ptr1->next);
                swapped = 1;
            }
            ptr1 = ptr1->next;
        }
        lptr = ptr1;
    }
    while (swapped);
}
 
/* function to swap data of two nodes a and b*/
void swap(struct MemNode *a, struct MemNode *b)
{
    char temp_physical_name[100];
    int64_t temp_physical_address;
    int64_t temp_size;

    strcpy(temp_physical_name ,a->physical_name);
    strcpy(a->physical_name, b->physical_name);
    strcpy(b->physical_name,temp_physical_name);

    temp_physical_address = a->physical_address;
    a->physical_address = b->physical_address;
    b->physical_address = temp_physical_address;

    temp_size = a->size;
    a->size = b->size;
    b->size = temp_size;

}


/*Translate logical address to physical using pageTable.
	Also checks permission.*/
int64_t translateAddress(int64_t i, FILE *pageTable, int64_t cr3OrEpt, int writable, int executable, int subjectType){

	const int64_t Bit_51_12 = 0x000ffffffffff000;
	const int64_t Bit_47_39 = 0x0000ff8000000000;
    const int64_t Bit_38_30 = 0x0000007fc0000000;
    const int64_t Bit_29_21 = 0x000000003fe00000;
    const int64_t Bit_20_12 = 0x00000000001ff000;
    const int64_t Bit_11_0  = 0x0000000000000fff;

    int64_t PML4E;
    int64_t PDPTE;
    int64_t PDE;
    uint64_t PTE;

    uint64_t Executable_Bit_Loc, Executable_Bit=0, Writable_Bit=0;

    int64_t PML4E_Address;
    int64_t PDPTE_Address;
    int64_t PDE_Address;
    int64_t PTE_Address;
    int64_t Data_Address;

    char tempChar;

    const uint64_t Bit_1 = 0x0000000000000002;

    if(subjectType == 0)
        //Native subject
        Executable_Bit_Loc = 0x8000000000000000;
     else
        Executable_Bit_Loc = 0x0000000000000004;
     

	PML4E_Address = find_offset(((cr3OrEpt & Bit_51_12)+((((i & Bit_47_39)) >> 36))),(cr3OrEpt & Bit_51_12));
    PML4E = getPageTableEntry(PML4E_Address, pageTable);
    SetBit(PTbitarray, PML4E_Address/8);


	PDPTE_Address = find_offset(((PML4E & Bit_51_12)+((((i & Bit_38_30)) >> 27))),(cr3OrEpt & Bit_51_12));
	PDPTE = getPageTableEntry(PDPTE_Address, pageTable);
    SetBit(PTbitarray,PDPTE_Address/8 );
    //printf("Location : %" PRIu64 "", PDPTE_Address);
   // exit(0);


	PDE_Address = find_offset(((PDPTE & Bit_51_12)+((((i & Bit_29_21)) >> 18))),(cr3OrEpt & Bit_51_12));
	PDE = getPageTableEntry(PDE_Address, pageTable);
    SetBit(PTbitarray,PDE_Address/8 );

	PTE_Address = find_offset(((PDE & Bit_51_12)+((((i & Bit_20_12)) >> 9))),(cr3OrEpt & Bit_51_12));
	PTE = getPageTableEntry(PTE_Address, pageTable);
    SetBit(PTbitarray,PTE_Address/8 );

	Data_Address = ((PTE & Bit_51_12)+((((i & Bit_11_0)) >> 0)));

	Executable_Bit = (PTE) & (Executable_Bit_Loc);

	if(subjectType == 0){
	 	//Native subject
	 	Executable_Bit = (Executable_Bit) >> 63;
	 	if(Executable_Bit != 1){
	 		if(executable != 1){
	 			printf("!!!  Permission Mismatch. (Executable permission) \n");
                // printf("Do you wish to continue (y/n)");
                // tempChar = getchar();
                // if(tempChar != 'y' ){
                //     return 0;
                // }
                // tempChar = getchar();
                CheckStopOnError();
            }
	 	}else{
	 		if(executable == 1){
	 			printf("!!! Permission Mismatch. (Executable permission) \n");
                // printf("Do you wish to continue (y/n)");
                // tempChar = getchar();
                // if(tempChar != 'y' ){
                //     return 0;
                // }
                // tempChar = getchar();
                CheckStopOnError();
	 		}
	 	}
	}else{
	 	Executable_Bit = (Executable_Bit) >> 2;
	 	if(Executable_Bit == 1){
	 		if(executable != 1){
                    printf("!!! Permission Mismatch. (Executable permission) \n");
                    // printf("Do you wish to continue (y/n)");
                    // tempChar = getchar();
                    // if(tempChar != 'y' ){
                    //     return 0;
                    // }
                    // tempChar = getchar();
                    CheckStopOnError();
	 			}
	 	}else{
	 		if(executable == 1){
                printf("!!! Permission Mismatch. (Executable permission) \n");
                // printf("Do you wish to continue (y/n)");
                // tempChar = getchar();
                // if(tempChar != 'y' ){
                //     return 0;
                // }
                // tempChar = getchar();
                CheckStopOnError();	 			
	 		}
	 	}
	}
	    
    Writable_Bit = ((PTE) & (Bit_1));

    if(Writable_Bit == 2){
    	if(writable != 1){
    		printf("!!! Permission Mismatch. (Writable permission) 1\n");
            // printf("Do you wish to continue (y/n)");
            // tempChar = getchar();
            // if(tempChar != 'y' ){
            //     return 0;
            // }
            // tempChar = getchar();
            CheckStopOnError();
        }
    }else{
    	if( (writable == 1) && (Writable_Bit == 0)){
    		
    		printf("!!! Permission Mismatch. (Writable permission) \n");
            // printf("Do you wish to continue (y/n)");
            // tempChar = getchar();
            // if(tempChar != 'y' ){
            //     return 0;
            // }
            // tempChar = getchar();
            CheckStopOnError();
    	}
    }
   
    return Data_Address;
}


int64_t find_offset(int64_t value, int64_t Base){
	return (value - Base);
}
   
int64_t getPageTableEntry(int64_t address, FILE *pageTable){

	int64_t value = 0x0000000000000000, decimalValue;
	char c1, c2, temp[17];
	int i = 0;
	
	if( fseek(pageTable, address*2, SEEK_SET) != 0){
    			printf("File seek error !");
    			exit(0);
    }

    for(i=8; i >0; i-- ){
    	//temp[i] = fgetc(pageTable);

    	c1 = fgetc(pageTable);
    	c2 = fgetc(pageTable);
    	temp[(i*2) - 1] = c2;
    	temp[i*2 - 2] = c1;

    	//printf("%c%c  ",c1, c2 );
    }
   
    temp[16] = '\0';
    //formatHex(temp);
    //printf("\n%s\n",temp );

    decimalValue = convtodecnum(temp);
    return decimalValue;
}      

/* Function to insert a node at the begining of a linked lsit */
void insertSubjectNode(struct SubjectNode **SubjectStart_ref, char PhysicalName[100],int64_t PhysicalAddress,
                        int64_t Size,char LogicalName[100],int64_t LogicalAddress,char Writable[50], 
                        char Executable[50], int SubjectID){

    struct SubjectNode *ptr1 = (struct SubjectNode*)malloc(sizeof(struct SubjectNode));
    strcpy(ptr1->PhysicalName ,PhysicalName);
    ptr1->PhysicalAddress = PhysicalAddress;
    ptr1->Size = Size;
    strcpy(ptr1->LogicalName ,LogicalName);
    ptr1->LogicalAddress = LogicalAddress;
    strcpy(ptr1->Writable ,Writable);
    strcpy(ptr1->Executable ,Executable);
    ptr1->SubjectID = SubjectID;


    ptr1->Next = *SubjectStart_ref;
    *SubjectStart_ref = ptr1;

}

/* Function to print nodes in a given linked list */
void printListSubjectNode(struct SubjectNode *start)
{
    struct SubjectNode *temp = start;
    //printf("\n");
    while (temp!=NULL)
    {
        printf("%s , ", temp->PhysicalName);
        printf("  %" PRId64 "\n", temp->PhysicalAddress);
        temp = temp->Next;
    }
}

/* Bubble sort the given linked lsit */
void bubbleSortSubjectNode(struct SubjectNode *SubjectStart)
{
    int swapped, i;
    struct SubjectNode *ptr1;
    struct SubjectNode *lptr = NULL;
 
    /* Checking for empty list */
    if (SubjectStart == NULL)
        return;
 
    do
    {
        swapped = 0;
        ptr1 = SubjectStart;
 
        while (ptr1->Next != lptr)
        {
            if (ptr1->PhysicalAddress >= ptr1->Next->PhysicalAddress)
            { 
                swapSubjectNode(ptr1, ptr1->Next);
                swapped = 1;
               // printf("\t\t Swapping");
            }
            ptr1 = ptr1->Next;
        }
        lptr = ptr1;
    }
    while (swapped);
}
 
/* function to swap data of two nodes a and b*/
void swapSubjectNode(struct SubjectNode *a, struct SubjectNode *b)
{
    char TLogicalName[100];
    int64_t TLogicalAddress;
    int64_t TPhysicalAddress;
    char TPhysicalName[100];
    int64_t TSize;
    char TWritable[50];
    char TExecutable[50];
    int TSubjectID;
    
    strcpy(TLogicalName, a->LogicalName);
    strcpy(a->LogicalName,b->LogicalName);
    strcpy(b->LogicalName,TLogicalName);

    TLogicalAddress = a->LogicalAddress;
    a->LogicalAddress = b->LogicalAddress;
    b->LogicalAddress = TLogicalAddress;

    TPhysicalAddress = a->PhysicalAddress;
    a->PhysicalAddress = b->PhysicalAddress;
    b->PhysicalAddress = TPhysicalAddress;

    strcpy(TPhysicalName, a->PhysicalName);
    strcpy(a->PhysicalName,b->PhysicalName);
    strcpy(b->PhysicalName,TPhysicalName);

    strcpy(TWritable, a->Writable);
    strcpy(a->Writable,b->Writable);
    strcpy(b->Writable,TWritable);

    strcpy(TExecutable, a->Executable);
    strcpy(a->Executable,b->Executable);
    strcpy(b->Executable,TExecutable);

     TSize = a->Size;
    a->Size = b->Size;
    b->Size = TSize;

     TSubjectID = a->SubjectID;
    a->SubjectID = b->SubjectID;
    b->SubjectID = TSubjectID;

}



int isChannelSegment(char physicalName[], xmlXPathContextPtr UxpathCtx){
	char tempString1[70];
	int number_of_channels;
	xmlXPathObjectPtr UxpathObj1;
	xmlNodeSetPtr UChannelElements;

	sprintf(tempString1, "/system/channels/channel[@name='%s']", physicalName);
    UxpathObj1 = xmlXPathEvalExpression(tempString1, UxpathCtx);

    if(UxpathObj1 == NULL) {
        fprintf(stderr,"Error: unable to evaluate xpath expression \"%s\"\n", tempString1);
        
        return -1;
    }

    UChannelElements= UxpathObj1->nodesetval;  
    number_of_channels =(UChannelElements) ? UChannelElements->nodeNr : 0;
    if(number_of_channels == 1){
    	return 1;
    }else{
    	return 0;
    }

}
